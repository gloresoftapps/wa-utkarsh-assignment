<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<style>
body {
  background-color: linen;
}

h1 {
  color: maroon;
  margin-left: 40px;
}
 .logoutcl{

   position:fixed;
   right:10px;
   top:5px;
}
</style>

<meta charset="ISO-8859-1">
<title>Landing Page</title>
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript">

	function getweatherInCityNonajax(){
	var text = document.getElementById('textInput');
		var city = encodeURIComponent(text.value);
		var form = document.createElement('form');
		form.setAttribute("action", "dashboard");
		form.setAttribute("method", "POST");

		var input = document.createElement("input");
		input.setAttribute("type", "hidden");
		input.setAttribute("name", "city");
		input.setAttribute("value", city);
		form.appendChild(input);

		$(input).appendTo(form);
		document.body.appendChild(form);
		form.submit();
	}
</script>
</head>
<body>

	<div align="center">
		<input type="text" id="textInput" placeholder="Enter City Name" /> <input
			type="button" value="Search" onclick="getweatherInCityNonajax();" />
	</div>
	<div class="logoutcl">
		<a href="/logout" class="logoutcl">logout</a>
	</div>
</body>
</html>