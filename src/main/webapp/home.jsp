<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<style>
body {
  background-color: linen;
}

h1 {
  color: maroon;
  margin-left: 40px;
}
 .logoutcl{

   position:fixed;
   right:10px;
   top:5px;
}
</style>
<meta charset="ISO-8859-1">
<title>Landing Page</title>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript">

	
	function getweatherInCityNonajax() {
		var text = document.getElementById('textInput');
		var city = encodeURIComponent(text.value);
		var form = document.createElement('form');
		form.setAttribute("action", "dashboard");
		form.setAttribute("method", "POST");
		var input = document.createElement("input");
		input.setAttribute("type", "hidden");
		input.setAttribute("name", "city");
		input.setAttribute("value", city);
		form.appendChild(input);
		$(input).appendTo(form);
		document.body.appendChild(form);
		form.submit(); 
	}
	
</script>
</head>
<body>
     <div align="center">
      <input type="text" id="textInput"  placeholder="Enter City Name"/>
      <input type="button" value="Search" onclick="getweatherInCityNonajax();" />
    </div>
	<table> 
	<c:choose>
	<c:when test="${'NoData' eq dataStatus }">
	<h2>No data found for this city</h2>
	</c:when>
	<c:otherwise>
		<thead>
			<tr class="stdHeader">
				<td>City Name</td>
				<td>Weather Description</td>
				<td>Current Temperature</td>
				<td>Min temperature</td>
				<td>Max temperature</td>
				<td>Sunrise</td>
				<td>Sunset</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="Histry" items="${datafromAPI}">
				<tr>
					 <td><input type="text" name="username" value="${Histry.c_n}"></td>
					<td><input type="text" name="username" value="${Histry.w_d}"></td>
					<td><input type="text" name="username" value="${Histry.c_t}"></td>
					<td><input type="text" name="username" value="${Histry.mi_t}"></td>
					<td><input type="text" name="username" value="${Histry.ma_t}"></td>
					<td><input type="text" name="username" value="${Histry.s_r}"></td>
					<td><input type="text" name="username" value="${Histry.s_s}"></td> 
				</tr>
			</c:forEach>
		</tbody>
		</c:otherwise>
	</c:choose>
	</table>
	<br><br><br><br>
	<h3>History</h3>
	
	<table id="editTableId">
	
			<thead>
			<tr class="stdHeader">
			    <td>City Name</td>
				<td>Weather Description</td>
				<td>Current Temperature</td>
				<td>Min temperature</td>
				<td>Max temperature</td>
				<td>Sunrise</td>
				<td>Sunset</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="Histry" items="${histry}" >
			<tr>
			<td> <input type="text" name="cn" id="cnid" value="${Histry.c_n}" /></td>
			<td> <input type="text" name="wd" id="wdid"  value="${Histry.w_d}"/></td>
			<td> <input type="text" name="ct" id="chkctname" value="${Histry.c_t}"/></td>
			<td> <input type="text" name="mit" id="chkminame" value="${Histry.mi_t}"/></td>
			<td> <input type="text" name="mat" id="chkmaname" value="${Histry.ma_t}"/></td>
			<td> <input type="text" name="sr" id="chksrname" value="${Histry.s_r}" /></td>
			<td> <input type="text" name="ss" id="chkssname" value="${Histry.s_s}"/></td> 
<%-- 	 <td><a href="edit/${Histry.id}/${Histry.c_n}/${Histry.w_d}/${Histry.c_t}/${Histry.mi_t}/${Histry.ma_t}/${Histry.s_r}/${Histry.s_s}">Edit</a></td> 
 --%> 	<td><a >Edit</a></td> 
  		<td><a href="delete/${Histry.id}/${selectedcity}">Delete</a></td> 
			</tr>
</c:forEach>
			</tbody>
	</table>
	
	<div class="logoutcl">
   <a href="/logout" class="logoutcl">logout</a>
  </div>
</body>
</html>