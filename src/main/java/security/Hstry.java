package security;

import javax.persistence.Entity;
/**
 * @author Utkarsh Mishra
 *
 */
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
@Entity
@SequenceGenerator(name="seq_hst", initialValue=1,allocationSize =1)
public class Hstry {
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_hst")
	@Id
	private long id;
	private String c_n;
	private String w_d;
	private String c_t;
	private String mi_t;
	private String ma_t;
	private String s_r;
	private String s_s;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getC_n() {
		return c_n;
	}
	public void setC_n(String c_n) {
		this.c_n = c_n;
	}
	public String getW_d() {
		return w_d;
	}
	public void setW_d(String w_d) {
		this.w_d = w_d;
	}
	public String getC_t() {
		return c_t;
	}
	public void setC_t(String c_t) {
		this.c_t = c_t;
	}
	public String getMi_t() {
		return mi_t;
	}
	public void setMi_t(String mi_t) {
		this.mi_t = mi_t;
	}
	public String getMa_t() {
		return ma_t;
	}
	public void setMa_t(String ma_t) {
		this.ma_t = ma_t;
	}
	public String getS_r() {
		return s_r;
	}
	public void setS_r(String s_r) {
		this.s_r = s_r;
	}
	public String getS_s() {
		return s_s;
	}
	public void setS_s(String s_s) {
		this.s_s = s_s;
	}
	@Override
	public String toString() {
		return "Histry [id=" + id + ", c_n=" + c_n + ", w_d=" + w_d + ", c_t=" + c_t + ", mi_t=" + mi_t + ", ma_t="
				+ ma_t + ", s_r=" + s_r + ", s_s=" + s_s + "]";
	}

}
