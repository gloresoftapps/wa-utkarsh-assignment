package security;

import java.util.List;
/**
 * @author Utkarsh Mishra
 *
 */

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
@Repository("ApiRepository")
public interface ApiRepository extends CrudRepository<Hstry, Long> {
	@Query("SELECT h.id FROM Hstry h where h.c_n = :name") 
    List<Long> findIdByName(@Param("name") String name);
//Hstry findByUserName (String username);
}
//public interface UserRepository extends JpaRepository<Usr, Long> {
//public interface UserRepository extends CrudRepository<Usr, Long> {

