package security;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
/**
 * @author Utkarsh Mishra
 *
 */
@Entity
@SequenceGenerator(name="seq_usr", initialValue=4,allocationSize =1)
public class Usr {
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_usr")
	@Id
	private long id;
	private String userName;
	private String password;
	private String email;
	private String dob;
	
	
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getDob() {
		return dob;
	}


	public void setDob(String dob) {
		this.dob = dob;
	}


	@Override
	public String toString() {
		return "Usr [id=" + id + ", userName=" + userName + ", password=" + password + ", email=" + email + ", dob="
				+ dob + "]";
	}
	
}
