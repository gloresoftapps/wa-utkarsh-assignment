package security;

import java.util.Optional;
/**
 * @author Utkarsh Mishra
 *
 */
public interface HistryService {
	public Iterable<Hstry> findAll();
	void delete(Long id);
	public Hstry update(Hstry hstry);
	 Optional<Hstry> findById(Long id);
	

	

}
