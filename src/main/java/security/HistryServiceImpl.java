package security;

/**
 * @author Utkarsh Mishra
 *
 */
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Transactional
@Service("HistryService")
public class HistryServiceImpl implements HistryService{

	@Autowired
	HistryRepository histryRepo;
	@Override
	public Iterable<Hstry> findAll() {
		return histryRepo.findAll();
	}
	@Override
	public void delete(Long id) {
		histryRepo.deleteById(id);
	}
	@Override
	public Hstry update(Hstry hstry) {
		return histryRepo.save(hstry);
	}
	@Override
	public Optional<Hstry> findById(Long id) {
		return histryRepo.findById(id);
	}
	
	
	

}
