package security;

import org.springframework.data.jpa.repository.JpaRepository;
/**
 * @author Utkarsh Mishra
 *
 */
import org.springframework.stereotype.Repository;
@Repository("UserRepository")
public interface UserRepository extends JpaRepository<Usr, Long> {
Usr findByUserName (String username);
}
