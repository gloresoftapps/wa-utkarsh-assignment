package security;

/**
 * @author Utkarsh Mishra
 *
 */
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository("HistryRepository")
public interface HistryRepository extends CrudRepository<Hstry, Long> {
}
