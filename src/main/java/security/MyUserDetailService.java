package security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
@Service
public class MyUserDetailService implements UserDetailsService {
	@Autowired(required = true)
	private security.UserRepository repo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usr usr = repo.findByUserName(username);
		if (usr == null) {
			throw new UsernameNotFoundException("User 404");
		}
		return new UserPrincipal(usr);
	}
	
	

}
