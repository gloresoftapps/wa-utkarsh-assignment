package security;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;
/**
 * @author Utkarsh Mishra
 *
 */
@ComponentScan("security")
@SpringBootApplication
public class WeatherInCityApplication {
    private static final Log LOG = LogFactory.getLog(HomeController.class);

	public static void main(String[] args) {
		SpringApplication.run(WeatherInCityApplication.class, args);
		LOG.debug("Spring boot application started ::: ");
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
}
