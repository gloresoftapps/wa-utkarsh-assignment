package security;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import modal.WeatherModel;
/**
 * @author Utkarsh Mishra
 *
 */
@ComponentScan(value = "security")
@Controller
public class HomeController {
    private static final Log LOG = LogFactory.getLog(HomeController.class);

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	HistryService histryService;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	ApiRepository apiRepo;
	@Autowired
	UserRepository usrRepo;
	@Value("${app.key}")
	private String key;
	@RequestMapping("/")

	public String home() {
		
		return "index.jsp";
	}

	@RequestMapping("/login")
	public String loginPage() {
		LOG.debug("loginPage /login ::: ");
		return "login.jsp";
	}

	@RequestMapping("/logout-success")
	public String logoutPage() {
		return "logout.jsp";
	}

	@RequestMapping("/dashboard" )
	public String getHomePage(Map<String, Object> model,HttpServletRequest request, HttpServletResponse response) {
		LOG.debug("getHomePage /dashboard ::: ");
		String cityName = request.getParameter("city");
		Hstry histryfromAPI = null;
		if (null != cityName) {
			histryfromAPI = getWeather(cityName, "N");
		}
		if (null == cityName) {
			HttpSession session = request.getSession();
			cityName = (String) session.getAttribute("city");
			if (null != cityName) {
				histryfromAPI = getWeather(cityName, "Y");
			}
		}
		LOG.debug("Selected City name::: "+cityName);
		if (histryfromAPI.getC_n() == null) {
			model.put("dataStatus", "NoData");
		}
		List<Hstry> listOfHistry = new ArrayList<>();
		listOfHistry.add(histryfromAPI);
		model.put("histry", histryService.findAll());
		model.put("datafromAPI", listOfHistry);
		model.put("selectedcity", histryfromAPI.getC_n());
		return "home.jsp";
	}
	
	public Hstry getWeather(String cityName,String isDeleteCall) {
		final String url =  "http://api.openweathermap.org/data/2.5/weather?q="+cityName+"&APPID="+key;
		 WeatherModel weather =null;
		try {
			  weather = restTemplate.getForObject(url, WeatherModel.class);
				LOG.debug("Rest API Data::: "+weather);

		}
		catch (Exception e) {
			LOG.error("Exception in API call::: "+e);
		}
		 Hstry hstry = new Hstry();
		 if(null!=weather) {
			float currentTemp=(float) ((weather.getMain().getTemp_min())-(273.15));
			 float minTemp=(float) ((weather.getMain().getTemp_min())-(273.15));
			 float maxTemp=(float) ((weather.getMain().getTemp_max())-(273.15));
			 Map<?, ?> wethMap = (Map<?, ?>) weather.getWeather().get(0);
			 String wethDesc= (String) wethMap.get("description");
			 hstry.setC_n(weather.getName());
			 hstry.setW_d(wethDesc);
			 hstry.setC_t(String.format("%.2f", currentTemp));
			 hstry.setMi_t(String.format("%.2f", minTemp));
			 hstry.setMa_t(String.format("%.2f", maxTemp));
			 hstry.setS_r(weather.getSys().getSunrise());
			 hstry.setS_s(weather.getSys().getSunset());
			 apiRepo.findIdByName(weather.getName());
			 String id="";
			 if(apiRepo.findIdByName(weather.getName()).size()>0) {
				  id=apiRepo.findIdByName(weather.getName()).toString();
					id = id.replace("[", "");
					id = id.replace("]", "").trim(); 
					hstry.setId(Long.parseLong(id));
			 }
				if ("N".equals(isDeleteCall)) {
					apiRepo.save(hstry);
				}
			}
		return hstry;
		 
		
	}
	@RequestMapping(value="/delete/{id}/{selectedcity}" ,method = RequestMethod.GET)
	public String deleteHistory(Map<String, Object> model,@PathVariable Long id,@PathVariable String selectedcity,HttpServletRequest request, HttpServletResponse response) {
		histryService.delete(id);
		model.put("histry", histryService.findAll());
		request.setAttribute("city", selectedcity);
		HttpSession session=request.getSession();
		session.setAttribute("city", selectedcity);
		return "redirect:/dashboard";
		
	}
	@RequestMapping(value="/edit/{id}/{c_n}/{w_d}/{c_t}/{mi_t}/{ma_t}/{s_r}/{s_s}")
	public String editHistoryRowData(@PathVariable Long id, @ModelAttribute("hstry") Hstry hstry,HttpServletRequest request, HttpServletResponse response) {
		System.out.println("edit caled");
		HttpSession session=request.getSession();
		session.setAttribute("hstry", hstry);	
		return "redirect:/editform";
		
	}

	@RequestMapping(value = "/editform")
	public String editHistoryShowForm(HttpServletRequest request, HttpServletResponse response,Map<String, Object> model) {
		HttpSession session = request.getSession();
		Hstry hstry = (Hstry) session.getAttribute("hstry");
		List<Hstry> listOfHistry = new ArrayList<>();
		listOfHistry.add(hstry);
		model.put("hstry", listOfHistry);
		return "editUpdate.jsp";
	}

	@RequestMapping(value = "/editformsave", method = RequestMethod.POST)
	public String editHistoryUpdateData(@ModelAttribute("userForm") Hstry hstry, BindingResult result) {
		System.out.println("inside editformsave......" + hstry);
		return "editUpdate.jsp";
	}

	@RequestMapping("/registration")
	public String registrationPage() {
		LOG.debug("registrationPage /registration ::: ");
		return "registration.jsp";
	}

	@RequestMapping(value = "/registrationSubmission")
	public String registrationSubmission(@ModelAttribute("usr") Usr usr) {
		LOG.debug("registrationSubmission /registrationSubmission ::: ");
		usr.setUserName(usr.getEmail());
		usr.setPassword(bCryptPasswordEncoder.encode(usr.getPassword()));
		boolean flagError = false;
		try {
			usrRepo.save(usr);

		} catch (Exception e) {
			flagError = true;
		}
		if (flagError) {
			LOG.debug("New User Registration Failed::: ");
			return "errorPage.jsp";
		} else {
			LOG.debug("New User Registration Successful completed::: ");
			return "login.jsp";
		}

	}

}