package modal;

public class Sys {
	 private float type;
	 private float id;
	 private String country;
	 private String sunrise;
	 private String sunset;


	 // Getter Methods 

	 public float getType() {
	  return type;
	 }

	 public float getId() {
	  return id;
	 }

	 public String getCountry() {
	  return country;
	 }

	 public String getSunrise() {
	  return sunrise;
	 }

	 public String getSunset() {
	  return sunset;
	 }

	 // Setter Methods 

	 public void setType(float type) {
	  this.type = type;
	 }

	 public void setId(float id) {
	  this.id = id;
	 }

	 public void setCountry(String country) {
	  this.country = country;
	 }

	 public void setSunrise(String sunrise) {
	  this.sunrise = sunrise;
	 }

	 public void setSunset(String sunset) {
	  this.sunset = sunset;
	 }

	@Override
	public String toString() {
		return "Sys [type=" + type + ", id=" + id + ", country=" + country + ", sunrise=" + sunrise + ", sunset="
				+ sunset + "]";
	}
	 
}