package modal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class WeatherEntity {
    
	@Id
	private String locName;
	private float temp;
	private float humidity;
	private float pressure;
	private float lon;
	private float lat;
	private float windSpeed;
	
	public float getWindSpeed() {
		return windSpeed;
	}
	public void setWindSpeed(float windSpeed) {
		this.windSpeed = windSpeed;
	}
	public String getLocName() {
		return locName;
	}
	public void setLocName(String locName) {
		this.locName = locName;
	}
	public float getTemp() {
		return temp;
	}
	public void setTemp(float temp) {
		temp= (float) (temp-273.15);
		this.temp = temp;
	}
	public float getHumidity() {
		return humidity;
	}
	public void setHumidity(float humidity) {
		this.humidity = humidity;
	}
	public float getPressure() {
		return pressure;
	}
	public void setPressure(float pressure) {
		this.pressure = pressure;
	}
	public float getLon() {
		return lon;
	}
	public void setLon(float lon) {
		this.lon = lon;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
}
