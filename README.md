# README #
This README would normally document whatever steps are necessary to get your application up and running.
### What is this repository for? ###
* Quick summary- 
  This is a web application, where user can see weather information over 200000 city in this world, just by entering city name.
* version: 0.0.1
### There are two way to run this application ###
### 1- Application run by doing local machine  set up (follow line no 12 -96) in notepad++ ###
### 2- Application  jar run  (follow line no 1 -24 (except line no 14), 34-86,99-103) in notepad++###
### How do I get set up? ###
* Summary of set up
For seting up application in your machine , below are needed as prerquisite.
1. jdk 1.8 or higher version of java
2. Eclipse/STS or any other workbench.
3. maven 3.3 or higher version.
4. Windows 8 or 10 or any higher operating system.
5. Oracle, mysql,H2 or any data RDBMS.

* Code check out
1. make a folder in local drive (Example: C:\Users\utkarsh\workapace)
2. open command prompt in this folder 
3. type command in command prompt  git init
4. Next type  git clone https://utk1081@bitbucket.org/abhijitingle/wa-utkarsh-assignment.git
5. If it ask for password enter password (this i can share in seperate email)

* Import code into Eclipse/STS
1. Open Eclipse/STS File>>import>>Existing Maven Projects>>select project>>Brows>>Finish
2. Update project dependency by right click on imported project>>maven>>update project.
3. Resolve maven dependency error if any.
* Configuration

* Dependencies
in Eclipse make sure pointing out to correct maven repository in setings.xml.
* Database configuration
1. In my case , i have used Oracle as RDBMS, and hence only need to update credential by your database in        \src\main\resources\application.properties file.
2. In case you have mysql data base then need to update pom.xml and add below entry and update properties file accordingly with credentials and url.
<dependency>
         <groupId>mysql</groupId>
         <artifactId>mysql-connector-java</artifactId>
         <version>5.1.34</version>
      </dependency>
	  
3. In case you have spring boot inbuild H2 data base then need to update pom.xml and add below entry and update properties file accordingly  with credentials and url
	<dependency>
		<groupId>com.h2database</groupId>
		<artifactId>h2</artifactId>
	</dependency>
	
 4.go to data base and run below query in databse
 --- (usr- i used this name because already USER table was there in my database)
-------------------1. create usr table----------------------------------------------
 CREATE TABLE USR(
  id int NOT NULL,
  user_name varchar(255) DEFAULT NULL,
  password varchar(255) DEFAULT NULL,
  email varchar(255) DEFAULT NULL,
  dob varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
CONSTRAINT UC_Person UNIQUE (email,dob)
); 
------------------2 create usr sequence------------------------------

  CREATE SEQUENCE seq_usr
  START WITH 1
  INCREMENT BY 1
  CACHE 100;
--History(HSTRY- i used this name because already History table was there in my database) Table 
------------------3----create HSTRY Table -----------------------------
CREATE TABLE HSTRY
( id int NOT NULL,
  c_n varchar(255) DEFAULT NULL,
  w_d varchar(255) DEFAULT NULL,
  c_t varchar(255) DEFAULT NULL,
  mi_t varchar(255) DEFAULT NULL,
  ma_t varchar(255) DEFAULT NULL,
  s_r varchar(255) DEFAULT NULL,
  s_s varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT hstry_unique UNIQUE (c_n)
);
--------------4----------------------HSTRY Table sequencer--------------------
CREATE SEQUENCE seq_hst
  START WITH 1
  INCREMENT BY 1
  CACHE 100;
 -----------------------------------------------------------------------------
* How to run tests
* Deployment instructions
1. We are almost done with configuration, its time to run the application and see user interface.
2. Go to \src\main\java\security\WeatherInCityApplication.java >>right click on this class>>run As>>Java Application.
3. Go to Browser (i.e Googlr chrome, IE, Mozila or any other) 
4. enter url http://localhost:8080/login
5. You will see user login screen, for the first time login go with registration (button at the right top) and detail.
6. After registration automatically you will be directed towards login page, please enter email id as user name and password.
7. In the next screen enter city name, you want to see weather detail, and submit , you will see dashboard.
8. You can delete the record from history(HSTRY) table.

### Application  jar run ###
1. If user want to  direct run jar file without doing all above configuration, follow below steps.
2. got to project workspace and copy jar into separate folder \target\weatherInCity-0.0.1-SNAPSHOT.jar
3. open command prompt and run       java -jar weatherInCity-0.0.1-SNAPSHOT.jar
4. after application is up and running open browser and enter url http://localhost:8080/login
5. Do registration and do user login and see application.

############## not completed and  in progress task ###################
Due to time constraint below functionality is still in progress.
1. edit records, Bulk delete and junit.

####Contact detail##########
for any assistence please drop me mail on utk.tit@gmail.com or call +91 7709173277

